﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core
{
    public class PromoCodeService
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public PromoCodeService(IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository, IRepository<PromoCode> promoCodesRepository)
        {
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _promoCodesRepository = promoCodesRepository;
        }

        public async Task<OperationResult> GivePromoCodesToCustomersWithPreferenceAsync(PromoCode promoCode)
        {
            var preference = await _preferencesRepository.GetByIdAsync(promoCode.PreferenceId);

            if (preference == null)
            {
                return OperationResult.Fail;
            }

            var customers = await _customersRepository
                .GetWhere(d => d.Preferences
                    .Any(x => x.Preference.Id == preference.Id));

            promoCode.Preference = preference;
            promoCode.Customers = customers
                .Select(x => new PromoCodeCustomer()
                {
                    CustomerId = x.Id,
                    Customer = x,
                    PromoCodeId = promoCode.Id,
                    PromoCode = promoCode
                })
                .ToList();

            await _promoCodesRepository.AddAsync(promoCode);

            return OperationResult.Success;
        }
    }
}
