﻿namespace Otus.Teaching.Pcf.GivingToCustomer.Core
{
    public enum OperationResult
    {
        Success,
        Fail,
        NotFound,
    }
}
