﻿using System;

namespace Otus.Teaching.Pcf.Messages
{
    public class PartnerManagerAppliedPromocodeMessage
    {
        public Guid PartnerManagerId { get; }

        public PartnerManagerAppliedPromocodeMessage(Guid partnerManagerId)
        {
            PartnerManagerId = partnerManagerId;
        }
    }
}