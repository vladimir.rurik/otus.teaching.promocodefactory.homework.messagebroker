﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core
{
    public class EmployeeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<OperationResult> UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return OperationResult.NotFound;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
            
            return OperationResult.Success;
        }
    }
}
