﻿namespace Otus.Teaching.Pcf.Administration.Core
{
    public enum OperationResult
    {
        Success,
        Fail,
        NotFound,
    }
}
